import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';
import {FontAwesome} from "@expo/vector-icons";
import {Button} from "react-native-elements";

export default function HomePage({ navigation }: RootTabScreenProps<'Home'>) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Home Page</Text>
            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
            {/* BTN element react bundle */}
            <Button
                onPress={() => navigation.navigate('CreateProject')}
                icon={
                    // use FontAwesome
                    <FontAwesome
                        name="plus"
                        size={40}
                        color={'white'}
                    />
                }
                buttonStyle={styles.button}
            />
        </View>
    );
}
// stylesheet
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    button: {
        flex: 1,
        minWidth: '75%',
        maxHeight: 75,
        backgroundColor: 'grey',
        justifyContent: 'center',
    }
});
